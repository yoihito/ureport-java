package tests;

import static org.junit.Assert.*;

import java.io.ObjectOutputStream.PutField;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import application.database.Database;
import application.database.Patient;

public class PatientTest {

	Database db;
	
	public PatientTest() throws Exception {
		db = new Database("test.db");
		//db.createTables();
	}

	@Before
	public void prepare() throws Exception {
		db.dropTables();
		db.createTables();
		for (int i = 0; i < 10; i++) {
			Patient patient = new Patient(new HashMap<String, Object>(){{
				Random rand = new Random();
				put("name", String.valueOf(rand.nextInt()));
				put("surname", String.valueOf(rand.nextInt()));
			}}
			);
			patient.save();
		}
	}
	
	@Test
	public void testFindByPk() throws Exception{
		Patient cur = (new Patient()).findByPk(1);
		cur.name="lah";
		cur.save();
	}

	@Test
	public void testSave() throws Exception{
		Patient cur = new Patient();
		cur.name="loh";
		cur.surname="lohov";
		cur.patronymic="lohovich";
		cur.birth_date=Long.valueOf((new Date()).getTime()).intValue();
		cur.sex="male";
		cur.save();
	} 
	
	@Test
	public void testSelectByValues() throws Exception {
		ArrayList<Patient> patients = (new Patient()).selectByValues(new HashMap<String,Object>());
		for (Patient p : patients) {
			System.out.println(p.name);
		}
	}

}
