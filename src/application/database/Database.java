package application.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.transform.Result;

public class Database {
	
	public static Connection c;
	
	private final String[] CREATETABLE = {
			"CREATE TABLE patients ("
			+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "name TEXT, "
			+ "surname TEXT, "
			+ "patronymic TEXT, "
			+ "birth_date INTEGER, "
			+ "sex TEXT);",
	};
	
	private final String[] DROPTABLE = {
			"DROP TABLE patients;",
	};
	
	public Database() {
		
	}
	
	public Database(String dbName) throws Exception {
		connect(dbName);
	}
	
	public void connect(String dbName) throws Exception {
		try {
			Class.forName("org.sqlite.JDBC");	
			c = DriverManager.getConnection("jdbc:sqlite:"+dbName);
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
		}
	}
	
	public void execute(String sql) throws Exception {
		try {
			Statement stmt = c.createStatement();
			stmt.execute(sql);
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
			
		}
	}
	
	
	// BUG!!
	public ArrayList<HashMap<String,Object>> executeQuery(String sql) {
		  try {

		    //create statement
		    Statement Stm = null;
		    Stm = c.createStatement();

		    //query
		    ResultSet Result = null;
		    boolean Returning_Rows = Stm.execute(sql);
		    if (Returning_Rows)
		      Result = Stm.getResultSet();
		    else
		      return new ArrayList<HashMap<String,Object>>();

		    //get metadata
		    ResultSetMetaData Meta = null;
		    Meta = Result.getMetaData();

		    //get column names
		    int Col_Count = Meta.getColumnCount();
		    ArrayList<String> Cols = new ArrayList<String>();
		    for (int Index=1; Index<=Col_Count; Index++)
		      Cols.add(Meta.getColumnName(Index));

		    //fetch out rows
		    ArrayList<HashMap<String,Object>> Rows = 
		    new ArrayList<HashMap<String,Object>>();

		    while (Result.next()) {
		      HashMap<String,Object> Row = new HashMap<String,Object>();
		      for (String Col_Name:Cols) {
		        Object Val = Result.getObject(Col_Name);
		        Row.put(Col_Name,Val);
		      }
		      Rows.add(Row);
		    }

		    //close statement
		    Stm.close();

		    //pass back rows
		    return Rows;
		  }
		  catch (Exception Ex) {
		    System.out.print(Ex.getMessage());
		    return new ArrayList<HashMap<String,Object>>();
		  }
		}
	
	public boolean exist() {
		return true;
	}
	
	public void createTables() throws Exception {
		try {
			for (String query : CREATETABLE) {
				execute(query);
			}
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
		}
		
	}
	
	public void dropTables() throws Exception {
		try {
			for (String query : DROPTABLE) {
				execute(query);
			}
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
		}
	}
	
	public Connection getConnection() {
		return c;
	}
		
}
