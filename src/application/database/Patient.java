package application.database;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import application.database.Model;


public class Patient extends Model {
	
	// id - 0
	// name - 1
	// surname - 2
	// patronymic - 3
	// birth_date - 4
	// sex - 5
	
	private final String FINDBYPK = 
			"SELECT * "
			+ "FROM patients "
			+ "WHERE id={0,number,#};";
	
	private final String INSERTREC = 
			"INSERT "
			+ "INTO patients (name,surname,patronymic,birth_date,sex) "
			+ "VALUES ( ''{1}'',''{2}'',''{3}'',{4,number,#},''{5}'' );"; 
	
	private final String UPDATEREC = 
			"UPDATE patients "
			+ "SET name=''{1}'', surname=''{2}'', patronymic=''{3}'', birth_date={4,number,#}, sex=''{5}'' "
			+ "WHERE id={0,number,#};";

	private final String SELECTBYVALUES = 
			"SELECT * "
			+ "FROM patients "
			+ "{0};";
	
	
	private Integer id;
	public String name;
	public String surname;
	public String patronymic;
	public Integer birth_date;
	public String sex;
	
	public Patient() {
		super();
		id = null;
	}
	
	public Patient(HashMap<String, Object> arg) {
		super();
		id = null;
		fillPatient(arg);
	}
		
	public Patient findByPk(Integer pk) throws Exception{
		try {
			ArrayList<HashMap<String,Object>> records=executeQuery(MessageFormat.format(FINDBYPK, pk));
			HashMap<String, Object> record = records.get(0);
			fillPatient(record);
			return this;
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
		}
	}
	
	public ArrayList<Patient> selectByValues(HashMap<String,Object> args) {
		String exprForSql = new String();
		Integer size = args.size();
		if (size>0) exprForSql="WHERE ";
		
		for (Entry<String, Object> entry : args.entrySet()) {
			String param1=entry.getKey(), param2=(String)entry.getValue();
			exprForSql+=param1+" = ''"+param2+"''";
			if (--size>0) {
				exprForSql+="AND ";
			}
		}
		
		
		ArrayList <HashMap<String, Object>> records=executeQuery(
				MessageFormat.format(SELECTBYVALUES, exprForSql));
		
		ArrayList <Patient> patients = new ArrayList<Patient>();
		for (HashMap<String, Object> row : records) {
			patients.add(new Patient(row));
		}
		
		return patients;
	}
	
	public void save() throws Exception {
		try {
			//String ids=null;
			//if (id != null)	ids =id.toString();
			Object[] row = {id,name,surname,patronymic,birth_date,sex};	
			if (id == null)
				execute((new MessageFormat(INSERTREC)).format(row));
			else 
				execute((new MessageFormat(UPDATEREC)).format(row));
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
		}
	}
	
	
	private void fillPatient(HashMap<String, Object> arg){
		if (arg.containsKey("id"))
			id = (Integer)arg.get("id");
		if (arg.containsKey("name"))
			name = (String)arg.get("name");
		if (arg.containsKey("surname"))
			surname = (String)arg.get("surname");
		if (arg.containsKey("patronymic"))
			patronymic = (String)arg.get("patronymic");
		if (arg.containsKey("birth_date")) 
			birth_date = (Integer)arg.get("birth_date");
		if (arg.containsKey("sex"))
			sex = (String)arg.get("sex");
	}
	
	public Boolean exists() {
		
		return false;
	}
}
