package application.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;


public class Model {
	
	protected final String insertTemplate = "INSERT INTO ${table} (${columns}) VALUES (${values});";
	protected final String checkTableForExistenceTemplate = 
			"SELECT * FROM sqlite_master WHERE name ='${table}' and type='table'; ";
	
	protected Connection c;
		
	public Model() {
		this.c = Database.c;
	}
	
	protected void execute(String sql) throws Exception {
		try {
			Statement stmt = c.createStatement();
			stmt.execute(sql);
			stmt.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			throw e;
			
		}
	}
	

	//query a full sql command
	public ArrayList<HashMap<String,Object>> executeQuery(String sql) {
	  try {

	    //create statement
	    Statement Stm = null;
	    Stm = c.createStatement();

	    //query
	    ResultSet Result = null;
	    boolean Returning_Rows = Stm.execute(sql);
	    if (Returning_Rows)
	      Result = Stm.getResultSet();
	    else
	      return new ArrayList<HashMap<String,Object>>();

	    //get metadata
	    ResultSetMetaData Meta = null;
	    Meta = Result.getMetaData();

	    //get column names
	    int Col_Count = Meta.getColumnCount();
	    ArrayList<String> Cols = new ArrayList<String>();
	    for (int Index=1; Index<=Col_Count; Index++)
	      Cols.add(Meta.getColumnName(Index));

	    //fetch out rows
	    ArrayList<HashMap<String,Object>> Rows = 
	    new ArrayList<HashMap<String,Object>>();

	    while (Result.next()) {
	      HashMap<String,Object> Row = new HashMap<String,Object>();
	      for (String Col_Name:Cols) {
	        Object Val = Result.getObject(Col_Name);
	        Row.put(Col_Name,Val);
	      }
	      Rows.add(Row);
	    }

	    //close statement
	    Stm.close();

	    //pass back rows
	    return Rows;
	  }
	  catch (Exception Ex) {
	    System.out.print(Ex.getMessage());
	    return new ArrayList<HashMap<String,Object>>();
	  }
	}
	
	
	public Patient findByPk(Integer pk) throws Exception{
		return null;
	}
	
	public ArrayList<Patient> selectByValues() {
		return null;
	}
	
	public void save() throws Exception {
		
	}
	
	
	
	public Boolean exists() {
		return false;
	}
	
}
