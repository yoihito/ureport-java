package application;


import application.database.Database;
import application.ui.UserInterface;

public class Application {
	
	
	public Database db;
	public UserInterface ui;
	
	public void run() {
		try {
			db = new Database("test.db");
			ui = new UserInterface(db);
			
			ui.show();
						
		} catch (Exception e) {
			System.err.println(e.getClass().getName()+": "+e.getMessage());
		}
	}


}
