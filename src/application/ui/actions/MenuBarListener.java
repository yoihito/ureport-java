package application.ui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import application.ui.AboutWindow;
import application.database.Database;

public class MenuBarListener implements ActionListener {
	
	private Database db;
	
	public MenuBarListener(Database db) {
		this.db=db;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		System.out.println(event.getActionCommand());
		if (event.getActionCommand().equals("About")) {
			new AboutWindow();
		} else
		if (event.getActionCommand().equals("Create")) {
			if (db.exist()) {
				try {
					db.createTables();
				} catch(Exception e) {
					System.err.println(e.getClass().getName()+": "+e.getMessage());
				}
			}
		}
	}

}
