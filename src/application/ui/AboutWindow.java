package application.ui;

import javax.swing.JDialog;
import javax.swing.JLabel;

public class AboutWindow extends NotifyWindow {
	
	private final static String notification = "Created by yoihito!"; 
	
	public AboutWindow() {
		super(notification,false);
		showWindow();
	}
	
}
