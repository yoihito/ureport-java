package application.ui;

import application.database.Database;
import application.ui.MainWindow;

public class UserInterface {
	
	private MainWindow mainWindow;
	
	public void show() {
		mainWindow.showWindow();
	}
	
	public UserInterface(Database db) {
		mainWindow = new MainWindow("ureport",db);
	}
}
