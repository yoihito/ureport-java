package application.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

public class NotifyWindow extends JDialog implements ActionListener{
	
	protected JLabel notify;
	
	public NotifyWindow(String notification, Boolean show) {
		super();
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		notify = new JLabel(notification);
		notify.setAlignmentX(Component.CENTER_ALIGNMENT);
		JButton okBut = new JButton("Ok");
		okBut.setAlignmentX(Component.CENTER_ALIGNMENT);
		okBut.addActionListener(this);
		add(notify);
		add(okBut);
		if (show) {
			showWindow();
		}
	}
	
	
	
	public void showWindow() {
		pack();
		setModal(true);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand().equals("Ok")) 
			this.dispose();
	}

}
