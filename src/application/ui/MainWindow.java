package application.ui;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;

import application.database.Database;
import application.ui.actions.MenuBarListener;

class MainWindow extends JFrame{
	private Database db; 
	
	MainWindow(String windowTitle, Database db) {
		super(windowTitle);
		
		this.db=db;
		
		setExtendedState( getExtendedState()|JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		createMenu();
		createToolbar();
	}
	
	private void createMenu() {
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem item;
		MenuBarListener menubarListener = new MenuBarListener(db);
		
		menuBar = new JMenuBar();
		menu = new JMenu("Database");
		item = new JMenuItem("Create");
		item.addActionListener(menubarListener);
		menu.add(item);
		menuBar.add(menu);
		
		menu = new JMenu("Help");
		item = new JMenuItem("About");
		item.addActionListener(menubarListener);
		menu.add(item);
		menuBar.add(menu);
		
		setJMenuBar(menuBar);
	}
	
	private void createToolbar() {
		JToolBar toolbar = new JToolBar();
		
		
		
	}
	
	public void showWindow() {
		setVisible(true);
		setResizable(false);
	}
	
	
}
